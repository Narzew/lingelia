Rails.application.routes.draw do
  resources :dictionaries
  resources :words
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  #
  root :to => 'course#index'
  get 'courses', to: 'course#index', as: 'courses'
  get 'courses/create', to: 'course#create', as: 'courses2'
  get 'courses/:course_id', to: 'lesson#index', as: 'lessons'
  # Course editor
  get 'courses/:course_id/edit', to: 'lesson#edit', as: 'lessons_edit'
  get 'courses/:course_id/:lesson_id/', to: 'content#index', as: 'content'
  get 'courses/:course_id/:lesson_id/edit', to: 'content#edit', as: 'content_edit'
  get 'courses/:course_id/:lesson_id/:element_id', to: 'content#show', as: 'element'
  get 'content/:course_id/', to: 'content#index', as: 'content2'
  get 'content/:course_id/:element_id/', to: 'content#show', as: 'element2'

  # Logowanie
  get 'users/login' => 'users#login'
  post 'users/login' => 'sessions#create'

  # Rejestracja
  get '/users/register' => 'users#register'
  post '/users/register' => 'users#create'

  # Wylogowanie
  get 'users/logout' => 'users#logout'

  # Reviews
  get 'reviews' => 'reviews#index'

  # Words
  post 'content/addtoreviews' => 'content#add_to_reviews'

  # Update lesson sequence
  post 'lesson/sequpdate' => 'lesson#sequence_update'

  # Add new lesson
  post 'lesson/addnewlesson' => 'lesson#add_new_lesson'

  # Add new content
  post 'content/addnewcontent' => 'content#add_new_content'

  # Update content
  patch 'content/updatecontent' => 'content#update_content'

  # Delete content
  delete 'content/deletecontent' => 'content#delete_content'

  # Update content sequence
  post 'content/sequpdate' => 'content#sequence_update'

  # Rename lesson
  patch 'lesson/rename' => 'lesson#rename'

  # Delete lesson
  delete 'lesson/delete' => 'lesson#delete'

  # Dictionary
  get 'dictionary' => 'dictionary#dictionary'
  get 'dictionary/search' => 'dictionary#search'
  post 'dictionary/search' => 'dictionary#search'

  # Writer
  get 'writer' => 'dictionary#writer'

end
