class CreateCourses < ActiveRecord::Migration[6.0]
  def change
    create_table :courses do |t|
      t.string :name
      t.string :description
      t.string :target_lang
      t.string :instr_lang
      t.string :img_url
      t.integer :min_difficulty
      t.integer :max_difficulty

      t.timestamps
    end
  end
end
