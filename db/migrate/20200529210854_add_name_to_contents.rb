class AddNameToContents < ActiveRecord::Migration[6.0]
  def change
    add_column :contents, :name, :string
    add_column :contents, :seq, :integer
  end
end
