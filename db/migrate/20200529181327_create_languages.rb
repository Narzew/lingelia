class CreateLanguages < ActiveRecord::Migration[6.0]
  def change
    create_table :languages do |t|
      t.string :name
      t.string :description
      t.string :lang_code

      t.timestamps
    end
  end
end
