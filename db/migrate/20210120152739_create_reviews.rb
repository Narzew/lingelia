class CreateReviews < ActiveRecord::Migration[6.1]
  def change
    create_table :reviews do |t|
      t.integer :user_id
      t.string :lang1
      t.string :lang2
      t.integer :word_id
      t.timestamp :last_review
      t.timestamp :next_review
      t.integer :review_count
      t.integer :points
      t.integer :total
      t.integer :good
      t.integer :bad

      t.timestamps
    end
  end
end
