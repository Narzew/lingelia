class AddStatsToWords < ActiveRecord::Migration[6.1]
  def change
    add_column :words, :last_review, :timestamp
    add_column :words, :next_review, :timestamp
    add_column :words, :current_step, :integer
    add_column :words, :points, :integer
    add_column :words, :total, :integer
    add_column :words, :good, :integer
    add_column :words, :bad, :integer
  end
end
