class CreateLessons < ActiveRecord::Migration[6.0]
  def change
    create_table :lessons do |t|
      t.integer :course_id
      t.string :name
      t.string :description
      t.string :img_url
      t.integer :seq

      t.timestamps
    end
  end
end
