class CreateWords < ActiveRecord::Migration[6.1]
  def change
    create_table :words do |t|
      t.string :lang1
      t.string :lang2
      t.string :word
      t.string :translation
      t.string :extra

      t.timestamps
    end
  end
end
