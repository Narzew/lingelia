class CreateContents < ActiveRecord::Migration[6.0]
  def change
    create_table :contents do |t|
      t.integer :course_id
      t.integer :lesson_id
      t.string :type
      t.string :content

      t.timestamps
    end
  end
end
