class Fixtypecolumn < ActiveRecord::Migration[6.1]
  def change
    rename_column :contents, :type, :datatype
  end
end
