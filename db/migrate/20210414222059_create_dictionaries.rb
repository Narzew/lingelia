class CreateDictionaries < ActiveRecord::Migration[6.1]
  def change
    create_table :dictionaries do |t|
      t.string :lang1
      t.string :lang2
      t.string :word
      t.string :translation
      t.string :type
      t.string :extra
      t.integer :status

      t.timestamps
    end
  end
end
