class ReviewsController < ApplicationController

=begin
Word table structure
id -> Word ID
lang1 -> target lang ID
lang2 -> learning lang ID
word -> Word
translation -> Translation of the word
extra -> Extra data attached to a word
created_at -> Default timestamp created_at
updated_at -> Default timestamp updated_at
last_review -> When last time the word was shown
next_review -> When this word will be shown next time
current_step -> Step of the review [important to generating new schedules]
points -> Points on that element
Total -> Total amount of reviews done
Good -> Good answers
Bad -> Bad answers
=end

  def index
    if  cookies[:course_id] == nil
      $selected_course_id = ""
    else
      $selected_course_id = cookies[:course_id]
    end

    if cookies[:lang1] != nil
      $lang1 = cookies[:lang1]
    else
      $lang1 = "en"
    end

    if cookies[:lang2] != nil
      $lang2 = cookies[:lang2]
    else
      $lang2 = "pl"
    end

    @words = Word.where("user_id = #{session[:user_id]}").where("next_review < ?",Time.now).where("lang1": $lang1).where("lang2": $lang2)
    #@words = Word.where("user_id = #{session[:user_id]}").where("next_review" < ?", 1.seconds.ago)

    render "index"
  end

end
