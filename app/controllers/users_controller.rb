class UsersController < ApplicationController

  $selected_course_id = "1"

  def index

  end

  def register_form
    render 'register'
  end

  def login_form
    render 'login'
  end

  def logout
    session[:user_id] = 0
    redirect_to "/"
  end

  def create
    user = User.new(user_params)
    if user.save
      session[:user_id] = user.id
      redirect_to "/"
    else
      flash[:register_errors] = user.errors.fill_messages
      redirect_to "/users/register"
    end
  end

  private
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
