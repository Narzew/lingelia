class DictionaryController < ApplicationController
  before_action :set_dictionary, only: %i[ show edit update destroy ]
  skip_before_action :verify_authenticity_token

  # GET /dictionaries or /dictionaries.json
  def index
    @dictionaries = Dictionary.all
  end

  # Search by name
  #
  def search
    @word = params[:word]
    @dictionaries = Dictionary.where(["lang1 = ? and lang2 = ? and status=1 and (word like ? or translation like ?)", "nk", "pl", "%"+@word+"%", "%"+@word+"%"])
    result = ""
    @dictionaries.each do |dictionary|
      result << "<li><h3><b>#{dictionary.word}</b> - #{dictionary.translation}</h3></li>"
    end
    result = "<ul>#{result}</ul>"
    render plain: result
  end

  # GET /dictionaries/1 or /dictionaries/1.json
  def show
  end

  # GET /dictionaries/new
  def new
    @dictionary = Dictionary.new
  end

  # GET /dictionaries/1/edit
  def edit
  end

  # POST /dictionaries or /dictionaries.json
  def create
    @dictionary = Dictionary.new(dictionary_params)

    respond_to do |format|
      if @dictionary.save
        format.html { redirect_to @dictionary, notice: "Dictionary was successfully created." }
        format.json { render :show, status: :created, location: @dictionary }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @dictionary.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dictionaries/1 or /dictionaries/1.json
  def update
    respond_to do |format|
      if @dictionary.update(dictionary_params)
        format.html { redirect_to @dictionary, notice: "Dictionary was successfully updated." }
        format.json { render :show, status: :ok, location: @dictionary }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @dictionary.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dictionaries/1 or /dictionaries/1.json
  def destroy
    @dictionary.destroy
    respond_to do |format|
      format.html { redirect_to dictionaries_url, notice: "Dictionary was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  # Show dictionary

  def dictionary
    render 'dictionary'
  end

  # Use writer
  def writer
    render 'writer'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dictionary
      @dictionary = Dictionary.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def dictionary_params
      params.permit(:dictionary).permit(:lang1, :lang2, :word, :translation, :type, :extra, :status)
    end
end
