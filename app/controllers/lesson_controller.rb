class LessonController < ApplicationController

  def load_lessons
    @course = Course.where id: params[:course_id]
    @course = @course.first
    if(cookies[:course_id] == nil)
      $selected_course_id = ""
    else
      $selected_course_id = cookies[:course_id]
    end
    course_id = params[:course_id]
    # Set cookie
    cookies[:course_id] = course_id
    course_data = Course.find(course_id)
    cookies[:lang1] = course_data[:target_lang]
    cookies[:lang2] = course_data[:instr_lang]
    @lessons = Lesson.where(course_id: course_id).order("seq")
  end

  def index
    load_lessons
    render "index"
  end

  def edit
    load_lessons
    render "edit"
  end

  def update_lessons_seq(course_id)
    @lessons = Lesson.where(course_id: course_id).order(seq: :asc)
    counter = 1
    @lessons.each do |lesson|
      lesson.seq = counter
      puts "Lesson #{lesson.id} new seq is #{counter}"
      counter += 1
      lesson.save
    end
  end

  def sequence_update
    @lesson = Lesson.where(id: params[:id], course_id: params[:course_id]).first
    @lesson.seq = params[:new_seq]
    @lesson.save
  end

  def add_new_lesson
    @lesson = Lesson.new
    @lesson.name = params[:lesson_name]
    @lesson.course_id = params[:course_id]
    count = Lesson.where(course_id: params[:course_id]).count
    @lesson.seq = count
    @lesson.save
  end

  def delete
    Lesson.destroy(params[:lesson_id])
  end

  def rename
    @lesson = Lesson.find(params[:lesson_id])
    @lesson.name = params[:new_name]
    @lesson.save
  end

  def word_params
    params.permit(:course_id, :id, :new_seq, :lesson_name, :new_name)
  end

end
