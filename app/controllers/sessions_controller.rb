class SessionsController < ApplicationController

  def create
    user = User.find_by(email:login_params[:email])
    if user && user.authenticate(login_params[:password])
      session[:user_id] = user.id
      session[:user_name] = user.name
      session[:user_email] = user.email
      redirect_to "/"
    else
      flash[:login_errors] = ['Invalid credentials']
      redirect_to "/users/login"
    end
  end

  private
  def login_params
    params.require(:login).permit(:email, :password)
  end

end
