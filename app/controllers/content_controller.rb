class ContentController < ApplicationController
  skip_before_action :verify_authenticity_token

  def load_contents
    @lesson = Lesson.where id: params[:lesson_id], course_id: params[:course_id]
    @course = Course.where id: params[:course_id]
    @lesson = @lesson.first
    @course = @course.first
    if (cookies[:course_id] == nil)
      $selected_course_id = ""
    else
      $selected_course_id = cookies[:course_id]
    end
    if params[:lesson_id] != nil
      @content = Content.where course_id: params[:course_id], lesson_id: params[:lesson_id]
    else
      if params[:content_type] == nil || params[:content_datatype] == "all"
        @content = Content.where course_id: params[:course_id]
      elsif params[:content_type] == "words"
        @content = Content.where course_id: params[:course_id], datatype: "wordlist"
      elsif params[:content_type] == "tutorials"
        @content = Content.where course_id: params[:course_id], datatype: "tutorial"
      elsif params[:content_type] == "phrases"
        @content = Content.where course_id: params[:course_id], datatype: "phrases"
      elsif params[:content_type] == "texts"
        @content = Content.where course_id: params[:course_id], datatype: "text"
      elsif params[:content_type] == "dialogues"
        @content = Content.where course_id: params[:course_id], datatype: "dialogue"
      elsif params[:content_type] == "exercises"
        @content = Content.where course_id: params[:course_id], datatype: "exercise"
      elsif params[:content_type] == "audios"
        @content = Content.where course_id: params[:course_id], datatype: "audio"
      elsif params[:content_type] == "videos"
        @content = Content.where course_id: params[:course_id], datatype: "video"
      end
    end
    @content = @content.order("seq")
    @content_datatype = 'list'
  end

  def index
    load_contents
    render "index"
  end

  def edit
    load_contents
    render "edit"
  end

  def show
    @content = Content.find_by course_id: params[:course_id], id: params[:element_id]
    @lesson_id = @content.lesson_id
    @seq = @content.seq
    @next_content = Content.where(course_id: params[:course_id], lesson_id: @lesson_id).order(:seq).where("seq > ?", @content.seq).first
    if @next_content == nil
      @next_element = 0
    else
      @next_element = @next_content.id
    end
    if @next_element != 0
      @next_element = "#{params[:course_id]}/#{@next_element}"
    end

    puts("DEBUG: #{@next_element}")
    print(@content)
    #@content = Content.find_by course_id: params[:course_id], lesson_id: params[:lesson_id], id: params[:element_id]
  end

  # POST /words
  # POST /words.json
  def add_to_reviews
    # Add specified word
    if session[:user_id] == nil || session[:user_id] == 0
      render status: :forbidden, text: "Can't add word to reviews. You must have been logged in."
    else
      # Do it only when creating a new review
      @mode = word_params[:mode]
      puts "WORD: #{word_params[:word]}"
      puts "TRANSLATION: #{word_params[:translation]}"
      puts "LANG1: #{word_params[:lang1]}"
      puts "LANG2: #{word_params[:lang2]}"
      puts "USER_ID: #{session[:user_id]}"

      @word = Word.find_or_create_by word: word_params[:word], translation: word_params[:translation], lang1: word_params[:lang1], lang2: word_params[:lang2], user_id: session[:user_id]
      puts "CURRENT_WORD"
      puts @word.to_s

      case @mode
      when "new"
        puts "REVIEW - NEW"
        # New entity
        @word.user_id = session[:user_id]
        @word.last_review = 0
        @word.next_review = Time.now + 1.hours
        @word.current_step = 0
        @word.points = 0
        @word.total = 0
        @word.good = 0
        @word.bad = 0
      when "good"
        puts "REVIEW - GOOD"
        # Good answer
        @word.last_review = Time.now
        @word.next_review = calculate_new_review(@word.current_step)
        @word.current_step += 1
        @word.points += @word.current_step
        @word.total += 1
        @word.good += 1
      when "bad"
        puts "REVIEW - BAD"
        # Bad answer
        @word.last_review = Time.now
        @word.next_review = calculate_new_review(0)
        @word.points -= 1
        @word.total += 1
        @word.bad += 1
      end

      @word.save
    end
  end

  # Calculate new review interval
  def calculate_new_review(review_step)
    review_step = review_step.to_i
    case review_step
    when 0 then return Time.now + 1.hours
    when 1 then return Time.now + 6.hours
    when 2 then return Time.now + 1.days
    when 3 then return Time.now + 2.days
    when 4 then return Time.now + 4.days
    when 5 then return Time.now + 7.days
    when 6 then return Time.now + 14.days
    when 7 then return Time.now + 28.days
    when 8 then return Time.now + 2.months
    when 9 then return Time.now + 6.months
    when 10 then return Time.now + 1.years
    when 11 then return Time.now + 2.years
    else
      return Time.now + 5.years
    end
  end

  def sequence_update
  end

  # POST
  def add_new_content
    @content = Content.new
    @content.course_id = params[:course_id]
    @content.lesson_id = params[:lesson_id]
    @content.name = params[:name]
    @content.datatype = params[:datatype]
    @content.content = params[:content]
    count = Content.where(lesson_id: params[:lesson_id]).count
    @content.seq = count
    @content.save
  end

  #PATCH
  def update_content
    @content = Content.find(params[:content_id])
    @content.name = params[:name]
    @content.datatype = params[:datatype]
    @content.content = params[:content]
    @content.save
  end

  #DELETE
  def delete_content
    @content = Content.destroy(params[:content_id])
  end

  def sequence_update
    @content = Content.where(id: params[:id], course_id: params[:course_id]).first
    @content.seq = params[:new_seq]
    @content.save
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_word
    @word = Word.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def word_params
    params.permit(:lang1, :lang2, :word, :translation, :extra, :mode, :course_id, :lesson_id, :name, :content, :datatype, :content_id)
  end

end