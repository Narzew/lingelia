class CourseController < ApplicationController

  def index
    if(cookies[:course_id] == nil)
      $selected_course_id = ""
    else
      $selected_course_id = cookies[:course_id]
    end
    # Filter courses or show all courses
    # No param: all courses
    # target_lang param -> filter by target lang
    # instr_lang param -> filter by instr lang
    if params[:target_lang] != nil and params[:instr_lang] != nil
      @courses = Course.where target_lang: params[:target_lang], instr_lang: params[:instr_lang]
    elsif params[:target_lang] != nil
      @courses = Course.where target_lang: params[:target_lang]
    elsif params[:instr_lang] != nil
    @courses = Course.where instr_lang: params[:instr_lang]
    else
      @courses = Course.all
    end
  end

  def create
    @languages = Language.all
  end

end
