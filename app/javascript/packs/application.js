// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
// require("turbolinks").start()
require("@rails/activestorage").start()
require("channels")
require("jquery");
require("jquery-ui-dist/jquery-ui");


// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)
import "@fortawesome/fontawesome-free/css/all"

// jquery
import $ from 'jquery';

global.$ = $
global.jQuery = $

// jquery-ui theme
// require.context('file-loader?name=[path][name].[ext]&context=node_modules/jquery-ui-dist!jquery-ui-dist', true,    /jquery-ui\.css/ );
// require.context('file-loader?name=[path][name].[ext]&context=node_modules/jquery-ui-dist!jquery-ui-dist', true,    /jquery-ui\.theme\.css/ )

import "bootstrap"
import "../stylesheets/application"

function updateIndices(){
    var listItems = $("#ul-lessons li");
    listItems.each(function (idx, li) {
        var item = $(li);
        var course_id = item.attr("data-course-id");
        var element_id = item.attr('id');
        // console.log("ID: " + item.attr('id') + "\nIndex: " + item.index());
        $.ajax({
            url: '/lesson/sequpdate',
            type: 'POST',
            data: { course_id: course_id, id: element_id, new_seq: item.index() },
            success: function(r){ return false; }
        });
    });
}

function updateContentIndices(){
    var listItems = $("#ul-content li");
    listItems.each(function (idx, li) {
        var item = $(li);
        var course_id = item.attr("data-course-id");
        var content_id = item.attr("data-content-id");
        // var element_id = item.attr('id');
        // console.log("ID: " + item.attr('id') + "\nIndex: " + item.index());
        $.ajax({
            url: '/content/sequpdate',
            type: 'POST',
            data: { course_id: course_id, id: content_id, new_seq: item.index() },
            success: function(r){ return false; }
        });
    });
}

function insertLesson(lessonName, courseId){
    updateIndices();
    // alert("Lesson name: "+lessonName+"\ncourseId: "+courseId);
    $.ajax({
        url: '/lesson/addnewlesson',
        type: 'POST',
        data: { course_id: courseId, lesson_name: lessonName},
        success: function(r){ location.reload(); }
    });
}

function editLesson(lessonId, courseId){
    // Do this
}

$(function () {
    // Plain jquery
    $('#fadeMe').fadeOut(5000);

    // jquery-ui
    const availableCities = ['Baltimore', 'New York'];
    $('#cityField').autocomplete({source: availableCities});
    $('#calendarField').datepicker({dateFormat: 'yy-mm-dd'});
    $('#ul-lessons').sortable({
        start: function (e, ui) {
            $(this).attr('data-previndex', ui.item.index());
        },
        update: function (e, ui) {
            var new_index = parseInt(ui.item.index())+1;
            var old_index = parseInt($(this).attr('data-previndex'))+1;
            var element_id = ui.item.attr('id');
            var course_id = ui.item.attr('data-course-id');

            $(this).removeAttr('data-previndex');
        }
    });
    $('#ul-content').sortable({
        start: function (e, ui) {
            $(this).attr('data-previndex', ui.item.index());
        },
        update: function (e, ui) {
            var new_index = parseInt(ui.item.index())+1;
            var old_index = parseInt($(this).attr('data-previndex'))+1;
            var element_id = ui.item.attr('id');
            var course_id = ui.item.attr('data-course-id');

            $(this).removeAttr('data-previndex');
        }
    });

    $("#save-data-button").on('click',function(){
        updateIndices();
        alert("Lesson data saved!");
    });
    $("#save-content-data-button").on('click',function(){
        updateContentIndices();
        alert("Content data saved!");
    });
    $("#add-new-lesson").on('click',function(){
        insertLesson($("#new-lesson-text").val(),$("#course-id").attr("data-course-id"));
    });
});

import "controllers"
