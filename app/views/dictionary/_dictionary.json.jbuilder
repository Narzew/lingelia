json.extract! dictionary, :id, :lang1, :lang2, :word, :translation, :type, :extra, :status, :created_at, :updated_at
json.url dictionary_url(dictionary, format: :json)
