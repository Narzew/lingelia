json.extract! word, :id, :lang1, :lang2, :word, :translation, :extra, :created_at, :updated_at
json.url word_url(word, format: :json)
